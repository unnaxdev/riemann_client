from .pb import proto_pb2 as pb
import socket
import struct
import ssl


HOST = '127.0.0.1'
PORT = 5555
TIMEOUT = 10.0


class RiemannUDPTransport():
    def __init__(self, host=HOST, port=PORT):
        self._host = host
        self._port = port

    def write(self, buffer): # needs cleanup, conn pool
        sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
        sock.settimeout(TIMEOUT)
        sock.sendto(buffer, (self._host, self._port))
        sock.close()

class RiemannTCPTransport():
    def __init__(self, host=HOST, port=PORT):
        self._host = host
        self._port = port

    def connect(self):
        sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        sock.settimeout(TIMEOUT)
        sock.connect((self._host, self._port))
        return sock

    def write(self, buffer):
        sock = self.connect()
        l = len(buffer)
        hdr = struct.pack('!I', l)
        sock.send(hdr)
        sock.send(buffer)
        r = sock.recv(4)
        hl = struct.unpack('!I', r)
        res = sock.recv(hl[0])
        c = pb.Msg().FromString(res)
        sock.close()
        return c

class RiemannTLSTransport(RiemannTCPTransport):
    def __init__(self, host=HOST, port=PORT, ca_certs=None, keyfile=None, certfile=None):
        """Communicates with Riemann over TCP + TLS

        Options are the same as :py:class:`.TCPTransport` unless noted

        :param str ca_certs: Path to a CA Cert bundle used to create the socket
        """
        super(RiemannTLSTransport, self).__init__(host, port)
        self.ca_certs = ca_certs
        self.keyfile = keyfile
        self.certfile = certfile

    def connect(self):
        """Connects using :py:meth:`TLSTransport.connect` and wraps with TLS"""
        sock = super(RiemannTLSTransport, self).connect()
        return ssl.wrap_socket(
            sock,
            keyfile=self.keyfile,
            certfile=self.certfile,
            ssl_version=ssl.PROTOCOL_TLSv1,
            cert_reqs=ssl.CERT_REQUIRED,
            ca_certs=self.ca_certs
        )

class RiemannClient():
    """
    Client to Riemann.
    RiemannClient constructor can receive the following parameters:
    - host
    - port
    - transport (RiemannUDPTransport, RiemannTCPTransport or any class that
      implements the write() method and receive both host and port on __init__())

    Example:
    from riemann import RiemannClient

    rc = RiemannClient()
    rc.send({'host':'127.0.0.1', 'service': 'www', 'state': 'down', 'metric': 10000})
    res = rc.query('host')
    print res
    for e in res.events: print e.host
    """
    def __init__(self, host=HOST, port=PORT, ca_certs=None, keyfile=None, certfile=None):
        if ca_certs and keyfile and certfile:
            transport = RiemannTLSTransport(host=host, port=port, ca_certs=ca_certs, keyfile=keyfile, certfile=certfile)
        else:
            transport = RiemannTCPTransport(host=host, port=port)

        self._transport = transport
        self._fields = ['description', 'host', 'service', 'state', 'time', 'ttl']

    def send(self, edict):
        ev = pb.Event()
        for k in self._fields:
            if k in edict: setattr(ev, k, edict[k])
        if 'attributes' in edict:
            attrs = []
            for k in edict['attributes']:
                attrib = pb.Attribute()
                setattr(attrib, 'key', k)
                setattr(attrib, 'value', edict['attributes'][k])
                attrs.append(attrib)
            ev.attributes.extend(attrs)
        if 'tags' in edict:
            ev.tags.extend(edict['tags'])
        if 'metric' in edict:
            ev.metric_f = float(edict['metric'])
        msg = pb.Msg()
        msg.events.extend([ev])
        b = msg.SerializeToString()
        return self._transport.write(b)

    def query(self, query):
        msg = pb.Msg()
        msg.query.string = str(query)
        return self._transport.write(msg.SerializeToString())

