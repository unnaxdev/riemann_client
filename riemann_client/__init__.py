import time
import os
import logging
from .client import RiemannClient
from threading import Thread
from collections import defaultdict

logger = logging.getLogger('riemann')

hostname = ''

try:
    hostname = os.uname()[1]
except:
    pass

rc = RiemannClient(host='10.8.0.13', port=5555)

# Send example
#rc.send({
#    'host': "Fitnance_worker_1",
#    'service': "fitnance.time_response",
#    'metric': 0.24,
#    'description': 'Fitnance response took 0.24 seconds.',
#    'attributes': {'worker-id': 'uuid-1', 'environment': 'pre'}
#    'tags': ['gauge']
#})

def make_send_thread(send_fn):
    def ignore_errors(fn):
        def fn_ignore(*args, **kwargs):
            try:
                return fn(*args, **kwargs)
            except:
                logger.exception("Riemann: metric error")
        return fn_ignore

    def send_thread(arg_send):
        thread = Thread(target=ignore_errors(send_fn), args=(arg_send, ))
        thread.daemon = True
        thread.start()
    return send_thread

class BaseMetric(object):
    def __init__(self, app, service, description='', rc_send=rc.send, opts={}, use_thread=True):
        self.base_opts = {
            'host': opts.get('host', hostname),
            'service': "{}.{}".format(app, service),
            'ttl': opts.get('ttl', 60)
        }

        self.base_attributes = opts.get('attributes', {})

        self.base_tags = list(opts.get('tags', []))  # Clone tags list

        if use_thread:
            self.rc_send = make_send_thread(rc_send)
        else:
            self.rc_send = rc_send

        if description:
            self.base_opts['description'] = description

    def send(self, opts={}):
        opts_attributes = opts.get('attributes', {})
        attributes = self.base_attributes.copy()
        attributes.update(opts_attributes)

        # Replace None by ''
        attributes = {key: attributes[key] if attributes[key] != None else '' for key in attributes}

        opts['attributes'] = attributes

        tags = opts.get('tags', [])
        opts['tags'] = list(set(tags + self.base_tags))

        arg_send = self.base_opts.copy()
        arg_send.update(opts)

        if 'description' in arg_send:
            format_args = attributes.copy()
            format_args['metric'] = opts.get('metric', '')
            arg_send['description'] = arg_send['description'].format_map(defaultdict(str, format_args))
        try:
            self.rc_send(arg_send)
            logger.info("Riemann: service %s", arg_send.get('service', ''))
        except Exception as e:
            logger.exception("Riemann: metric error. %s", str(e))


class Gauge(BaseMetric):
    def report(self, value=1, state='ok', attributes={}):
        opts = {
            'state': state,
            'tags': [],
            'metric': value,
            'attributes': attributes
        }
        try:
            self.send(opts)
        except Exception:
            logger.exception("Fail to report to riemann")

    def report_error(self, value=1, attributes={}):
        self.report(value=value, state='error', attributes=attributes)

    def report_warning(self, value=1, attributes={}):
        self.report(value=value, state='warning', attributes=attributes)

    def report_exception(self, value=1, exception="Exception", attributes={}):
        report_attributes = attributes.copy()
        report_attributes['exception'] = exception
        self.report(value=value, state='exception', attributes=report_attributes)

def request_time(metric, attributes={}, validation=None):
    def decorator_func(func):
        def response_func(*args, **kwargs):
            job_state = 'error'
            start_time = time.time()
            try:
                response = func(*args, **kwargs)
                if validation:
                    job_state = validation(response)
                else:
                    job_state = 'ok'
            except Exception as e:
                # TODO report exception
                raise
            finally:
                elapsed_time = (time.time() - start_time) * 1000 # ms
                metric.report(value=elapsed_time, state=job_state, attributes=attributes)
            return response
        return response_func
    return decorator_func


#Gauge EXAMPLE:
# fitnance_time_response = Gauge('fitnance', 'time_response', 'Fitnance response: [{metric}] secs')
# fitnance_time_response.report(10)

#request_time EXAMPLE:
# dummy_time_response = Gauge('dummy', 'time_response', 'time response in secs', environment='pre')
# @request_time(dummy_time_response, attributes={'dummy_attr': 'T_T'}, validation=lambda result: 'ok' if result else 'error')
# def dummy_fnc():
#     print("Doing dummy things")
#     return False # Returning false send the event state as error
