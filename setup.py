from setuptools import setup

setup(name='riemann_client',
      version='1.0.9',
      description='Riemann metric Client',
      url='https://bitbucket.org/unnaxdev/riemann_client',
      author='Ramon A. Roldan',
      author_email='raroldan@unnax.com',
      license='MIT',
      packages=['riemann_client', 'riemann_client.pb'],
      zip_safe=False,
      install_requires=[
          'protobuf',
      ])
